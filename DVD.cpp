#include "DVD.h"
#include <sstream>


DVD::DVD(std::string kind, int id, std::string name, double price, int quantity, double sales, std::string director,
    int released, std::string genre) : Product(kind, id, name, price, quantity, sales)
{
    this->director = director;
    this->released = released;
    this->genre = genre;
}

DVD::DVD() :Product()
{
    director = "";
    released = 0;
    genre = "";
}

DVD::~DVD() { std::cout << "de-constructor : DVD\n"; }

// Seters
std::string DVD::getDirector() { return this->director; }
int DVD::getReleased() { return this->released; }
std::string DVD::getGenre() { return this->genre; }
// Getters
void DVD::setDirector(std::string director) { this->director = director; }
void DVD::setReleased(int released) { this->released = released; }
void DVD::setGenre(std::string genre) { this->genre = genre; }

/*
  write_to_file function, which takes int param (id) to 
  allocate the item if exist, else write item members to the file.
  @param id to allocate the item if exist. 
*/
void DVD::write_to_file(int id)
{
    std::string kind = "DVD";
    std::string name, director, genre;
    int quantity, released;
    double price, sales;

    std::cout << "insert item name : ";
    std::cin >> name;
    std::cout << "insert item price : ";
    std::cin >> price;
    std::cout << "insert item Qty : ";
    std::cin >> quantity;
    std::cout << "insert item sales : ";
    std::cin >> sales;
    //+++++++++++++++++++++++++
    std::cout << "insert director : ";
    std::cin >> director;
    std::cout << "insert released year (mmmm): ";
    std::cin >> released;
    std::cout << "insert genre : ";
    std::cin >> genre;

    std::ofstream outputFile;
    outputFile.open("stock.dat", std::ios::out | std::ios::app);

    outputFile << kind << "\t" << id << "\t" << name << "\t" << price << "\t" << quantity << "\t" << sales
        << "\t" << director << "\t" << released << "\t" << genre << "\n";

    outputFile.close();
    std::cout << "\nProduct '" << name << "' saved successfully!\n\n";
}
