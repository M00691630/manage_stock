#include "Product.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <sstream>
#include "CD.h"
#include "DVD.h"
#include "Book.h"
#include "Magazine.h"

// default Product Constructor 
Product::Product()
{
    this->kind = "";
    this->id = 0;
    this->name = "";
    this->price = 0.0;
    this->quantity = 0;
    this->sales = 0.0;
}

// Product Constructor
Product::Product(std::string kind, int id, std::string name, double price, int quantity, double sales)
{
    this->kind = kind;
    this->id = id;
    this->name = name;
    this->price = price;
    this->quantity = quantity;
    this->sales = sales;
}
// Product De-Constructor
Product::~Product() {     }

// Getters
int Product::getId() { return id; }
std::string Product::getKind() { return kind; }
std::string  Product::getName() { return name; }
double Product::getPrice() { return price; }
int Product::getQuantity() { return quantity; }
double Product::getSales() { return sales; }

// Setters
void Product::setId(int id) { this->id = id; }
void Product::setKind(std::string kind) { this->kind = kind; }
void Product::setName(std::string name) { this->name = name; }
void Product::setPrice(double price) { this->price = price; }
void Product::setQuantity(int quantity) { this->quantity = quantity; }
void Product::setSales(double sales) { this->sales = sales; }

/*
  productExist function checks if the product is exist in the file or not.
  @param id to search product.
  @return boolean value, true if the product is exist.
*/
bool Product::productExist(int id)
{
    std::vector<std::string> vec = get_product_vector(id);
    if (vec.empty())
        return false;
    else
        return true;
}

/*
  get the item from the stock file.
  @param id to search item.
  @return vector, which contains the returned item.
*/
std::vector<std::string> Product::get_product_vector(int id)
{
    std::vector<std::string> vec;
    std::ifstream inputFile("stock.dat");
    bool is_found = false;
    std::string productArray[6];
    std::string line;
    while (getline(inputFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }
        std::istringstream iss(line);
        for (int i = 0; i < (sizeof(productArray) / sizeof(productArray[0])); i++)
        {
            std::string subs;
            iss >> subs;
            productArray[i] = subs;
        }
        if (std::to_string(id) == productArray[1])
        {
            is_found = true;
            vec.push_back(productArray[0]);
            vec.push_back(productArray[1]);
            vec.push_back(productArray[2]);
            vec.push_back(productArray[3]);
            vec.push_back(productArray[4]);
            vec.push_back(productArray[5]);
        }
    }
    inputFile.close();
    return vec;
}

void Product::update_sales(int id, int sold_items_count)
{
    std::vector<std::string> vec = get_product_vector(id);
    std::string pKind = vec[0];

    bool isExist = false;
    std::ifstream inputFile("stock.dat");
    std::ofstream temporary_file("temp.dat");
    int vectorSize = 0;
    std::vector<std::string> product_Vec;

    if (pKind == "CD") { vectorSize = 10; }
    else if (pKind == "DVD") { vectorSize = 9; }
    else if (pKind == "Book") { vectorSize = 8; }
    else if (pKind == "Magazine") { vectorSize = 7; }

    product_Vec.resize(vectorSize);
    std::string line;
    while (getline(inputFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < product_Vec.size(); i++)
        {
            std::string subs;
            iss >> subs;
            product_Vec[i] = subs;
        }
        if (id != stoi(product_Vec[1]) && line.length() > 1)
            temporary_file << line << "\n";
        else if (id == stoi(product_Vec[1]) && line.length() > 1)
        {
            isExist = true;
            double new_sales = stod(product_Vec[5]);

            new_sales = new_sales + sold_items_count * (stod(product_Vec[3]));
            product_Vec[5] = std::to_string(new_sales);

            std::string product_str = "";
            for (int i = 0; i < product_Vec.size(); i++)
            {
                if (i != product_Vec.size() - 1)
                    product_str += product_Vec[i] + "\t";
                else
                    product_str += product_Vec[i] + "\n";
            }
            temporary_file << product_str;

        }
    }
    inputFile.close();
    temporary_file.close();

    if (!isExist)
    {
        std::cout << "\n\t\t<<< No Product under this id (" << id << ") ! >>>\n";
        remove("temp.dat");
    }
    else
    {
        remove("stock.dat");
        int kk = rename("temp.dat", "stock.dat");
        std::cout << "\n\t\t<<< Done ! >>>\n";
    }
}

/*
  sell_item function, decreases the item quantity and increases the sales.
  @param id which used to retrieve the item
  @param sold_items_count represent the sold quantity of the item.
*/
void Product::sell_item(int id, int sold_items_count)
{
    update_sales(id, sold_items_count);
    restock_item(id, 0 - sold_items_count);
}

std::string Product::increase_quantity(std::vector<std::string> vect , int addition_quantity)
{
    int new_qty = 0;
    new_qty = stoi(vect[4]);
    new_qty += stoi(std::to_string(addition_quantity));
    vect[4] = std::to_string(new_qty);

    std::string product_str = "";
    for (int i = 0; i < vect.size(); i++)
    {
        if (i != vect.size() - 1)
            product_str += vect[i] + "\t";
        else
            product_str += vect[i] + "\n";
    }
    return product_str;

}
/*
    restock_item function will decrease the item quantity and increase the sales.
    @param id which used to get the item.
    @param addition_quantity represent the quantity need to add to the old one.
*/
void Product::restock_item(int id, int addition_quantity)
{
    std::vector<std::string> vec = get_product_vector(id);
    std::string pKind = vec[0];

    bool isExist = false;
    std::ifstream inputFile("stock.dat");
    std::ofstream temporary_file("temp.dat");
    int vectorSize = 0;
    std::vector<std::string> product_Vector;

    if (pKind == "CD") { vectorSize = 10; }
    else if (pKind == "DVD") { vectorSize = 9; }
    else if (pKind == "Book") { vectorSize = 8; }
    else if (pKind == "Magazine") { vectorSize = 7; }

    product_Vector.resize(vectorSize);
    std::string line;
    while (getline(inputFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }

        std::istringstream iss(line);
        for (int i = 0; i < product_Vector.size(); i++)
        {
            std::string subs;
            iss >> subs;
            product_Vector[i] = subs;
        }
        if (id != stoi(product_Vector[1]) && line.length() > 1)
            temporary_file << line << "\n";
        else if (id == stoi(product_Vector[1]) && line.length() > 1)
        {
            isExist = true;
            std::string product_str = increase_quantity(product_Vector, addition_quantity);
            temporary_file << product_str;
        }
    }
    inputFile.close();
    temporary_file.close();

    if (!isExist)
    {
        std::cout << "\n\t\t<<< No Product under this id (" << id << ") ! >>>\n";
        remove("temp.dat");
    }
    else
    {
        remove("stock.dat");
        int kk = rename("temp.dat", "stock.dat");
    }
}

/*
    in this function we will decrease the quantity of the item and increase the sales
    @param id which used to retrieve the item
    @param quantity represent the quantity need to store instead of old one.
*/
void Product::updateStockItem(int id, int quantity)
{
    std::vector<std::string> vec = get_product_vector(id);
    int addition_quantity = stoi(std::to_string(quantity)) - stod(vec[4]);
    restock_item(id, addition_quantity);
}

/*
  add_new_product which take an integer number as id of product,
  it adds new product to the file, by calling function write_to_file(id)
  @param id which represent the id of product to add it to the file.
*/
void Product::add_new_product(int id)
{
    if (!productExist(id))
    {
        int product_type_num;
        std::cout << "1) Add 'CD' item\n";
        std::cout << "2) Add 'DVD' item\n";
        std::cout << "3) Add 'Magazine' item\n";
        std::cout << "4) Add 'Book' item\n";
        std::cout << "\t choose a number (1, 2, 3 or 4) : ";
        std::cin >> product_type_num;
        while (product_type_num < 1 && product_type_num>4)
        {
            std::cout << "\t choose a valid number (1, 2, 3 or 4) : ";
            std::cin >> product_type_num;
        }

        if (product_type_num == 1)
        {
            CD item;
            Product* prod = &item;
            prod->write_to_file(id);
        }
        if (product_type_num == 2)
        {
            DVD item;
            Product* prod = &item;
            prod->write_to_file(id);
        }
        if (product_type_num == 3)
        {
            Magazine item;
            Product* prod = &item;
            prod->write_to_file(id);
        }
        if (product_type_num == 4)
        {
            Book item;
            Product* prod = &item;
            prod->write_to_file(id);
        }
    }
}

/*
  display sales report for all products, or for one type
  it calls prepare_report(kind) function.
*/
void Product::view_sales_report()
{
    int choice;
    std::string kind = "";
    std::cout << "\t1) CDs sales report\n";
    std::cout << "\t2) DVDs sales report\n";
    std::cout << "\t3) magazines sales report\n";
    std::cout << "\t4) books sales report\n";
    std::cout << "\t5) All sales report\n";
    std::cout << "\n\t Select report number to make it, (1, 2, 3, 4, 5): ";
    std::cin >> choice;
    if (choice == 1) kind = "CD";
    else if (choice == 2) kind = "DVD";
    else if (choice == 3) kind = "Magazine";
    else if (choice == 4) kind = "Book";
    else if (choice == 5) kind = "All";

    std::cout << "\n\t+++++++++++++++++++++++++++++++\n";
    std::cout << "\t   Sales report for " << kind << "  \n";
    std::cout << "\t+++++++++++++++++++++++++++++++\n\n";
    std::cout << std::setw(8) << "ID" << std::setw(14) << "Name" << std::setw(16) << "Sales\n";
    std::cout << std::setw(8) << "--" << std::setw(14) << "----" << std::setw(16) << "-----\n";
    prepare_report(kind);
}

/*
  prepare report depending on kind of product or for all products,
  @param kind which contains the kind of product or 'All' for all products
*/
void Product::prepare_report(std::string kind)
{
    std::ifstream inputFile("stock.dat");
    double total = 0.0;
    std::vector<std::string> product_vect(6, "");
    std::string line;
    while (getline(inputFile, line))
    {
        if (line.length() < 1)
        {
            break;
        }
        std::istringstream word(line);
        for (int i = 0; i < product_vect.size(); i++)
        {
            std::string str;
            word >> str;
            product_vect[i] = str;
        }
        std::cout << std::fixed << std::showpoint << std::setprecision(2);
        if (kind == product_vect[0] && line.length() > 1 && stod(product_vect[5]) > 0)
        {
            total += stod(product_vect[5]) / 1;
            std::cout << std::setw(8) << product_vect[1] << "\t" << std::setw(8) << product_vect[2]
                << "\t" << std::setw(8) << stod(product_vect[5]) / 1 << "\n";
            product_vect[5] = "0";
        }
        else if (kind == "All" && stod(product_vect[5]) > 0)
        {
            total += stod(product_vect[5]) / 1;

            std::cout << std::setw(8) << product_vect[1] << "\t" << std::setw(8) << product_vect[2]
                << "\t" << std::setw(8) << stod(product_vect[5]) / 1 << "\n";
            product_vect[5] = "0";
        }
    }
    std::cout << "++++++++++++++++++++++++++++++++++++++++++\n";

    std::cout << "\tTotal Sales for '" << kind << "'  = " << total << "pounds GBP\n\n";
    inputFile.close();
}
