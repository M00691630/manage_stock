#ifndef CD_H
#define CD_H
#include "Product.h"

#include <fstream>

class CD : public Product
{
private:
    std::string artist;
    std::string album;
    int released;
    std::string genre;
public:
    
    CD(std::string kind, int id, std::string name, double price, int quantity, double sales,
        std::string artist, std::string album, int released, std::string genre);

    CD();

    ~CD();

    void write_to_file(int id);

    // Getters.
    std::string getArtist();
    std::string getAlbum();
    int getReleased();
    std::string getGenre();
    // Setters
    void setArtist(std::string artist);
    void setAlbum(std::string album);
    void setReleased(int released);
    void setGenre(std::string genre);


};

#endif // CD_H
