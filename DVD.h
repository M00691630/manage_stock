#ifndef DVD_H
#define DVD_H
#include "Product.h"
#include <fstream>

class DVD : public Product
{
private:
    std::string director;
    int released;
    std::string genre;
public:
    
    DVD(std::string kind, int id, std::string name, double price, int quantity,
        double sales, std::string director, int released, std::string genre);
    DVD();
    ~DVD();

    void write_to_file(int id);

    // Getters
    std::string getDirector();
    int getReleased();
    std::string getGenre();

    // Setters
    void setDirector(std::string director);
    void setReleased(int released);
    void setGenre(std::string genre);
};

#endif 
