#ifndef PRODUCT_H
#define PRODUCT_H
#include <iostream>
#include <string>
#include <vector>
#include <iomanip>

class Product
{
private:
    std::string kind;
    int id;
    std::string name;
    double price;
    int quantity;
    double sales;
public:
    Product();

    Product(std::string kind, int id, std::string name, double price, int quantity, double sales);
    
    ~Product();
    //
    void add_new_product(int id);
    virtual void write_to_file(int id) = 0;
    void sell_item(int id, int sold_items_count);
    void update_sales(int id, int sold_items_count);
    void updateStockItem(int id, int addition_quantity);
    void restock_item(int id, int addition_quantity);
    std::string increase_quantity(std::vector<std::string> vect, int addition_quantity);
    bool productExist(int id);
    std::vector<std::string> get_product_vector(int id);
    void view_sales_report();
    void prepare_report(std::string kind);

    // Getters
    int getId();
    std::string getKind();
    std::string  getName();
    double getPrice();
    int getQuantity();
    double getSales();

    // Setters
    void setId(int id);
    void setKind(std::string kind);
    void setName(std::string name);
    void setPrice(double price);
    void setQuantity(int quantity);
    void setSales(double sales);
};

#endif
