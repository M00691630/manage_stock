#include "Book.h"
#include <sstream>
//#include <iomanip>

Book::Book(std::string kind, int id, std::string name, double price, int quantity, double sales,
    std::string ISDN, int pageCount) : Product("Book", id, name, price, quantity, sales)
{
    this->ISDN = ISDN;
    this->pageCount = pageCount;
}

Book::Book() :Product()
{
    ISDN = "";
    pageCount = 0;
}

Book::~Book(){ std::cout << "de-constructor : Book\n"; }

// Getter
std::string Book::get_ISDN() { return this->ISDN; }
int Book::get_pageCount() { return this->pageCount; }

// Setter
void Book::set_ISDN(std::string ISDN) { this->ISDN = ISDN; }
void Book::set_pageCount(int pageCount) { this->pageCount = pageCount; }

/*
  write_to_file function, which takes int param (id) to 
  allocate the item if exist, else write item members to the file.
  @param id to allocate the item if exist.
*/
void Book::write_to_file(int id)
{
    std::string kind = "Book";
    std::string name, ISDN;
    int quantity, pageCount;
    double price, sales;

    std::cout << "insert item name : ";
    std::cin >> name;
    std::cout << "insert item price : ";
    std::cin >> price;
    std::cout << "insert item Qty : ";
    std::cin >> quantity;
    std::cout << "insert item sales : ";
    std::cin >> sales;
    //+++++++++++++++++++++++++++
    std::cout << "insert ISDN : ";
    std::cin >> ISDN;
    std::cout << "insert pageCount : ";
    std::cin >> pageCount;
    

    std::ofstream outputFile;
    outputFile.open("stock.dat", std::ios::out | std::ios::app);

    outputFile << kind << "\t" << id << "\t" << name << "\t" << price << "\t" << quantity << "\t" << sales
        << "\t" << ISDN << "\t" << pageCount << "\n";

    outputFile.close();
    std::cout << "\nProduct '" << name << "' saved successfully!\n\n";
}
