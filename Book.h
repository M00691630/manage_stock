//#pragma once
#ifndef BOOK_H
#define BOOK_H
#include "Product.h"
#include <fstream>

class Book : public Product
{
private:
    std::string ISDN;
    int pageCount;
public:
    
    Book(std::string kind, int id, std::string name, double price, int quantity, double sales,
        std::string ISDN, int pageCount);

    void write_to_file(int id);

    Book();
    ~Book();

    // Getter.
    std::string get_ISDN();
    int get_pageCount();

    // Setter
    void set_ISDN(std::string ISDN);
    void set_pageCount(int pageCount);
};
#endif 
