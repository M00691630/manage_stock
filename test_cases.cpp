#define CATCH_CONFIG_MAIN

#include "Product.h"
#include "catch.hpp"
#include "CD.h"
#include "DVD.h"
#include "Magazine.h"
#include "Book.h"

// four test cases, one for every class.

TEST_CASE("CD class Test", "[CD()]")
{
	CD cd("CD", 1, "loveSongs", 36.5, 20, 365, "Bowie", "Heroes", 2010, "Rock");

	REQUIRE(cd.getKind() == "CD");
	REQUIRE(cd.getId() == 1);
	REQUIRE(cd.getName() == "loveSongs");
	REQUIRE(cd.getPrice() == 36.5);
	REQUIRE(cd.getQuantity() == 20);
	REQUIRE(cd.getSales() == 365);
	REQUIRE(cd.getArtist() == "Bowie");
	REQUIRE(cd.getAlbum() == "Heroes");
	REQUIRE(cd.getReleased() == 2010);
	REQUIRE(cd.getGenre() == "Rock");

	cd.setId(2);
	cd.setName("thanks");
	cd.setPrice(40.2);
	cd.setQuantity(37);
	cd.setSales(700.8);
	cd.setArtist("Jack");
	cd.setAlbum("friends");
	cd.setReleased(2011);
	cd.setGenre("Pop");

	REQUIRE(cd.getId() == 2);
	REQUIRE(cd.getName() == "thanks");
	REQUIRE(cd.getPrice() == 40.2);
	REQUIRE(cd.getQuantity() == 37);
	REQUIRE(cd.getSales() == 700.8);
	REQUIRE(cd.getArtist() == "Jack");
	REQUIRE(cd.getAlbum() == "friends");
	REQUIRE(cd.getReleased() == 2011);
	REQUIRE(cd.getGenre() == "Pop");
}

TEST_CASE("DVD class Test", "[DVD()]")
{
	DVD dvd;
	dvd.setKind("DVD");
	dvd.setId(3);
	dvd.setName("Blue");
	dvd.setPrice(66.7);
	dvd.setQuantity(82);
	dvd.setSales(95000);
	dvd.setDirector("Derek");
	dvd.setReleased(1993);
	dvd.setGenre("adventure");

	REQUIRE(dvd.getKind() == "DVD");
	REQUIRE(dvd.getId() == 3);
	REQUIRE(dvd.getName() == "Blue");
	REQUIRE(dvd.getPrice() == 66.7);
	REQUIRE(dvd.getQuantity() == 82);
	REQUIRE(dvd.getSales() == 95000);
	REQUIRE(dvd.getDirector() == "Derek");
	REQUIRE(dvd.getReleased() == 1993);
	REQUIRE(dvd.getGenre() == "adventure");
}

TEST_CASE("Magazine class Test", "[Magazine()]")
{
	Magazine mg;
	Product* product = &mg;

	product->setKind("Magazine");
	product->setId(503);
	product->setName("Glamour");
	product->setPrice(15.3);
	product->setQuantity(90);
	product->setSales(2000.9);

	mg.setEdition(70012);

	REQUIRE(mg.getId() == 503);
	REQUIRE(mg.getKind() == "Magazine");
	REQUIRE(mg.getName() == "Glamour");
	REQUIRE(mg.getPrice() == 15.3);
	REQUIRE(product->getQuantity() == 90);
	REQUIRE(product->getSales() == 2000.9);
	REQUIRE(mg.getEdition() == 70012);

}

TEST_CASE("Book class Test", "[Book()]")
{
	Book book;
	book.setKind("Book");
	book.setId(4);
	book.setName("");
	book.setPrice(40.6);
	book.setQuantity(150);
	book.setSales(4000.1);
	book.set_pageCount(184);
	book.set_ISDN("BV0014");


	REQUIRE(book.getId() == 4);
	REQUIRE(book.getName() == "");
	REQUIRE(book.getKind() == "Book");
	REQUIRE(book.getPrice() == 40.6);
	REQUIRE(book.getQuantity() == 150);
	REQUIRE(book.getSales() == 4000.1);

	REQUIRE(book.get_ISDN() == "BV0014");
	REQUIRE(book.get_pageCount() == 184);

}
