#include "Magazine.h"
#include <sstream>


Magazine::Magazine(std::string kind, int id, std::string name, double price, int quantity, double sales,
    int edition) : Product(kind, id, name, price, quantity, sales)
{ this->edition = edition;}

Magazine::Magazine() :Product()
{edition = 0;}

Magazine::~Magazine(){ std::cout << "de-constructor : magazine\n";}

// Getter
int Magazine::getEdition() { return this->edition; }

// Setter.
void Magazine::setEdition(int edition) { this->edition = edition; }

/*
  write_to_file function, which takes int param (id) to 
  allocate the item if exist, else write item members to the file.
  @param id to allocate the item if exist.
*/
void Magazine::write_to_file(int id)
{
    std::string kind = "Magazine";
    std::string name;
    int quantity, edition;
    double price, sales;

    std::cout << "insert item name : ";
    std::cin >> name;
    std::cout << "insert item price : ";
    std::cin >> price;
    std::cout << "insert item Qty : ";
    std::cin >> quantity;
    std::cout << "insert item sales : ";
    std::cin >> sales;
    //+++++++++++++++++++++++++
    std::cout << "insert edition of Magazine : ";
    std::cin >> edition;

    std::ofstream outputFile;
    outputFile.open("stock.dat", std::ios::out | std::ios::app);
    outputFile << kind << "\t" << id << "\t" << name << "\t" << price << "\t" << quantity << "\t" << sales
        << "\t" << edition << "\n";

    outputFile.close();
    std::cout << "\nProduct '" << name << "' saved successfully!\n\n";
}
