// Stock Management System
#include <iostream>
#include <fstream>
#include <sstream>
#include <limits>
#include <string>
#include <iomanip>
#include <vector>
#include "Product.h"
#include "CD.h"
#include "DVD.h"
#include "Book.h"
#include "Magazine.h"

void print_main_menu();

// Main function
int main()
{
    Product* p = 0;
    int choice = -1;
    int id;
    // main menu
    while (choice != 6)
    {
        print_main_menu();
        std::cin >> choice;
        while (choice < 1 || choice> 6)
        {
            std::cout << "Not a Valid.. Enter a valid number [1 , 6] : ";
            std::cin >> choice;
        }
        // the choice for sell items, which decrease quantity and increase sales.
        if (choice == 1)
        {
            std::cout << "\n++++++++++ Sell items ++++++++++\n";
            std::cout << "The id of the item: ";
            std::cin >> id;

            int sold_items_count;
            std::cout << "\nThe number of sold items ? : ";
            std::cin >> sold_items_count;

            p->sell_item(id, sold_items_count);
            //p->restock_item(id, 0 - sold_items_count);
        }
        // the choice for increase quantity (restock_item).
        else if (choice == 2)
        {
            std::cout << "\n++++++++++++ Restock item ++++++++++++\n";
            std::cout << "\nThe id of the item ? : ";
            std::cin >> id;

            int addition_quantity;
            std::cout << "\nThe quantity to increase ? : ";
            std::cin >> addition_quantity;
            p->restock_item(id, addition_quantity);
        }
        // the choice for Add new item to the stock.
        else if (choice == 3)
        {
            int id;
            std::cout << "\n++++++++++++ Add new item ++++++++++++\n";
            std::cout << "\nThe id of the item ? : ";
            std::cin >> id;
            if (p->productExist(id))
            {
                std::cout << "\n\tThe Product of id = [" << id << "] already Exists!\n";
            }
            else
            {
                p->add_new_product(id);
            }
        }
        // the choice for correct stock item .
        else if (choice == 4)
        {
            std::cout << "\n++++++++ Update stock item +++++++++\n";
            std::cout << "\nid of the item? : ";
            std::cin >> id;
            while (!std::cin)
            {
                // if input is not a number
                std::cin.clear();
                // skip invalid input 
                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                std::cout << "\nid of the item? : ";
                std::cin >> id;
            }

            if (p->productExist(id))
            {
                std::cout << "\nEnter new Qty: ";
                int quantity;
                std::cin >> quantity;
                while (!std::cin)
                {
                    // if input is not a number
                    std::cin.clear();
                    // skip invalid input
                    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    std::cout << "\nEnter new Qty: ";
                    std::cin >>quantity;
                }
                p->updateStockItem(id, quantity);
            }
            else
                std::cout << "\n\t\t<<< No Product under this id (" << id << ") ! >>>\n";
        }
        // the choice for  displaying sales reports.
        else if (choice == 5)
        {
            std::cout << "\n+++++++++++ View report of sales +++++++++++\n\n";
            p->view_sales_report();
        }
    }
}

/* display Menu*/
void print_main_menu()
{
    std::cout << "\n\t||+++++++++++++++++++++++++++++++++++++++++++++++++++|| \n";
    std::cout << "\t||+++++++++++++ Stock Management System +++++++++++++||\n";
    std::cout << "\t||+++++++++++++++++++++++++++++++++++++++++++++++++++|| \n\n";
    std::cout << "\t1-  Sell items\n";
    std::cout << "\t2-  Restock items\n";
    std::cout << "\t3-  Add new items\n";
    std::cout << "\t4-  Update stock quantity (Correct Stock Level)\n";
    std::cout << "\t5-  View report of sales\n";
    std::cout << "\t6-  Exit the program\n\n";
    std::cout << "\t\tSelect choice by insert number : ";
}
