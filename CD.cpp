#include "CD.h"
#include <sstream>

// CD Constructor
CD::CD(std::string kind, int id, std::string name, double price, int quantity, double sales,
    std::string artist, std::string album, int released, std::string genre)
    :Product(kind, id, name, price, quantity, sales)
{
    this->artist = artist;
    this->album = album;
    this->released = released;
    this->genre = genre;
}

// default CD Constructor 
CD::CD() :Product()
{
    this->artist = "";
    this->album = "";
    this->released = 0;
    this->genre = "";
}

// CD De-Constructor
CD::~CD() { std::cout << "de-constructor : CD\n"; }

// Seters
std::string CD::getArtist() { return this->artist; }
std::string CD::getAlbum() { return this->album; }
int CD::getReleased() { return this->released; }
std::string CD::getGenre() { return this->genre; }

// Getters
void CD::setArtist(std::string artist) { this->artist = artist; }
void CD::setAlbum(std::string album) { this->album = album; }
void CD::setReleased(int released) { this->released = released; }
void CD::setGenre(std::string genre) { this->genre = genre; }

/*
  write_to_file function, which takes int param (id) to 
  allocate the item if exist, else write item members to the file.
  @param id to allocate the item if exist.
*/
void CD::write_to_file(int id)
{
    std::cout << std::fixed << std::showpoint << std::setprecision(2);

    std::string kind = "CD";
    std::string name, artist, album, genre;
    int quantity, released;
    double price, sales;

    std::cout << "insert item name : ";
    std::cin >> name;
    std::cout << "insert item price : ";
    std::cin >> price;
    std::cout << "insert item Qty : ";
    std::cin >> quantity;
    std::cout << "insert item sales : ";
    std::cin >> sales;
    //+++++++++++++++++++++++++
    std::cout << "insert artist : ";
    std::cin >> artist;
    std::cout << "insert album : ";
    std::cin >> album;
    std::cout << "insert released year (mmmm): ";
    std::cin >> released;
    std::cout << "insert genre : ";
    std::cin >> genre;

    std::ofstream outputFile;
    outputFile.open("stock.dat", std::ios::out | std::ios::app);

    outputFile << kind << "\t" << id << "\t" << name << "\t" << price << "\t" << quantity << "\t" << sales
        << "\t" << artist << "\t" << album << "\t" << released << "\t" << genre << "\n";

    outputFile.close();
    std::cout << "\nProduct '" << name << "' saved successfully!\n\n";
}
