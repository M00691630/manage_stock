CXX = g++
CXXFLAGS = -g -Wall -Wextra

.PHONY : all
all : stock_system

stock_system : stock_system.cpp Product.o CD.o DVD.o Magazine.o Book.o
	$(CXX) $(CXXFLAGS) -o $@ $^

CD.o : CD.cpp CD.h Product.o
	$(CXX) $(CXXFLAGS) -c $^

DVD.o : DVD.cpp DVD.h Product.o
	$(CXX) $(CXXFLAGS) -c $^

Book.o : Book.cpp Book.h Product.o
	$(CXX) $(CXXFLAGS) -c $^

Magazine.o : Magazine.cpp Magazine.h Product.o
	$(CXX) $(CXXFLAGS) -c $^

Product.o : Product.cpp Product.h
	$(CXX) $(CXXFLAGS) -c $<

.PHONY : clean
clean :
	rm *~ *.o stock_system
 
