#ifndef MAGAZINE_H
#define MAGAZINE_H
#include "Product.h"
#include <fstream>

class Magazine : public Product
{
private:
    int edition;
public:
    
    Magazine(std::string kind, int id, std::string name, double price, int quantity, double sales,
        int edition);
    Magazine();
    ~Magazine();

    void write_to_file(int id);

    // Getter
    int getEdition();

    // Setter.
    void setEdition(int edition);
};
#endif 
